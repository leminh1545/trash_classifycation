import os

def change_name():
    root_dir = 'additional_dataset2/'
    folders = [files for _, files, _ in os.walk(root_dir)][0]
    os.chdir(root_dir)
    
    for folder in folders:
        for _, _, files in os.walk(folder):
            for i, name in enumerate(files):
                os.rename(os.path.join(folder, files[i]), os.path.join(folder, folder + '_' + str(i+1)+'.jpg'))

def main():
    change_name()
if __name__ == '__main__':
    main()