import os
import constants
import numpy as np
# from scipy import misc, ndimage
import cv2

def resize(image, height, width):
	return cv2.resize(image, (height, width))

def fileWalk(directory, destPath):
	try: 
		os.makedirs(destPath)
	except OSError:
		if not os.path.isdir(destPath):
			raise

	for subdir, dirs, files in os.walk(directory):
		for file in files:
			if len(file) <= 4 or file[-4:] != '.jpg':
				continue
			try:
				pic = cv2.imread(os.path.join(subdir, file),cv2.IMREAD_UNCHANGED)
				h = pic.shape[0]
				w = pic.shape[1]
				if (h > w):
					pic = np.rot90(pic)

				picResized = resize(pic,constants.HEIGTH, constants.WIDTH)
				cv2.imwrite(os.path.join(destPath, file), picResized)
			except:
				print(file)

def main():
	prepath = os.path.join(os.getcwd(), 'additional_dataset2')
	cardboardDir = os.path.join(prepath, 'cardboard')
	glassDir = os.path.join(prepath, 'glass')
	paperDir = os.path.join(prepath, 'paper')
	plasticDir = os.path.join(prepath, 'plastic')
	metalDir = os.path.join(prepath, 'metal')
	trashDir = os.path.join(prepath, 'trash')
	destPath = os.path.join(os.getcwd(), 'addtional_dataset2_resized')
	try: 
		os.makedirs(destPath)
	except OSError:
		if not os.path.isdir(destPath):
			raise

	#CARDBOARD
	fileWalk(cardboardDir, os.path.join(destPath, 'cardboard'))
	#GLASS
	fileWalk(glassDir, os.path.join(destPath, 'glass'))

	#PAPER
	fileWalk(paperDir, os.path.join(destPath, 'paper'))

	#PLASTIC
	fileWalk(plasticDir, os.path.join(destPath, 'plastic'))

	#METAL
	fileWalk(metalDir, os.path.join(destPath, 'metal'))

	#TRASH
	fileWalk(trashDir, os.path.join(destPath, 'trash'))
if __name__ == '__main__':
    main()