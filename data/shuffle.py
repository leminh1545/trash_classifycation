import argparse
import os
import random
from glob import glob

def get_arguments():
    parser = argparse.ArgumentParser(description='Shuffle')
    parser.add_argument('--root_dir', type=str, default='addtional_dataset_resized/')
    parser.add_argument('--train', type=float, default=0.7)
    parser.add_argument('--val', type=float, default=0.13)
    parser.add_argument('--test', type=float, default=0.17)
    return parser.parse_args()

def split_data(args):
	class_name = ['glass', 'paper', 'cardboard', 'plastic', 'metal', 'trash']
	root_dir = args.root_dir

	# filelist = [files for _, _, files in os.walk(root_dir)][1:]

	def label_data(root_dir):
		with open('trash_index.txt', 'w') as f:
			for index, name in enumerate(class_name):
				filelist = glob(os.path.join(root_dir, name, '*.jpg'))
				for file in filelist:
					f.write(file.split('/')[-1] + ' ' + str(index + 1) + '\n')
	label_data(root_dir)
	train_rate = args.train
	val_rate = args.val
	test_rate = args.test

	with open('trash_index.txt', 'r') as f:
		files = f.read().splitlines()
	with open('train_index.txt', 'w') as train:
		with open('val_index.txt', 'w') as val:
			with open('test_index.txt', 'w') as test:
				# for label, files in enumerate(filelist):
				# 	files = [file + ' ' + str(label+1) for file in files]

				random.shuffle(files)
				num_train = int(train_rate * len(files))
				num_val = int(val_rate * len(files))
				
				for train_data in files[:num_train]:
					train.write(train_data + '\n')

				for val_data in files[num_train:num_train + num_val]:
					val.write(val_data + '\n')

				for test_data in files[num_train + num_val:]:
					test.write(test_data + '\n')

def main():
	args = get_arguments()
	split_data(args)

if __name__ == '__main__':
	main()