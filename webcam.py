import argparse
import torch
import torch.nn as nn
import os
import resnet
import cv2
import matplotlib.pyplot as plt
import pandas as pd
from torch.autograd import Variable
from torchvision import transforms
from PIL import Image

TRASH_DICT = {
'1' : 'glass',
'2' : 'paper',
'3' : 'cardboard',
'4' : 'plastic',
'5' : 'metal',
'6' : 'trash'
}

parser = argparse.ArgumentParser(description='RecycleNet webcam inference')
parser.add_argument('--resume', default='save/model_best.pth.tar', type=str)
parser.add_argument('--gpu', type=str, help='0; 0,1; 0,3; etc', required=True)
# parser.add_argument('--root_dir', default= 'data', type= str)
parser.add_argument('--data_dir', default= 'addtional_dataset2_resized', type= str)

args = parser.parse_args()

GPU = args.gpu
os.environ['CUDA_VISIBLE_DEVICES'] = GPU
if torch.cuda.is_available():
    print('using Cuda devices, num:', torch.cuda.device_count())
    device = torch.device('cuda')
else:
    device = torch.device('cpu')

# model = nn.DataParallel(resnet.resnet18(pretrained=True, num_classes=len(TRASH_DICT)).to(device))
# model = nn.DataParallel(resnet.resnet18(pretrained=True, num_classes=len(TRASH_DICT),
#                                         use_att=True, att_mode='ours').to(device))
model = nn.DataParallel(resnet.resnet18(pretrained=True, num_classes=len(TRASH_DICT),
                                        use_att=True, att_mode='cbam').to(device))
checkpoint = torch.load(args.resume, map_location=device)
state_dict = checkpoint['state_dict']
model.load_state_dict(state_dict)
model.eval()
print(model) 
print('Number of model parameters: {}'.format(
    sum([p.data.nelement() for p in model.parameters()])))
if args.resume:
    if os.path.isfile(args.resume):
        print("=> loading checkpoint '{}'".format(args.resume))
        args.start_epoch = checkpoint['epoch']
        best_acc1 = checkpoint['best_acc1']
        print("=> loaded checkpoint '{}' (epoch {})"
                .format(args.resume, checkpoint['epoch']))
        print('=> best accuracy {}'.format(best_acc1))
    else:
        print("=> no checkpoint found at '{}'".format(args.resume))

def predict_images(img_paths, annotations):
    print('.....Prediction in progress.....')
    total = len(img_paths)
    correct = 0; number = 1
    f = open('inference_result.txt', 'a') #write accurancy of model_test
    result = {}
    Number = []
    Img_name = []
    Class = []
    Predict = []
    Correct = []
    Confidence = []
    for img_path, annotation in zip(img_paths, annotations):
        # img = cv2.imread(img_path)
        # img_resized = cv2.resize(img, (224, 224), interpolation=cv2.INTER_LINEAR)
        # cv2.cvtColor(img_resized, cv2.COLOR_BGR2RGB)
        # plt.show()
        try:
            img_name = img_path.split('/')[-1]
            image = Image.open(img_path)
            
            MEAN = [0.485, 0.456, 0.406]
            STD = [0.229, 0.224, 0.225]
            
            img_transforms = transforms.Compose([
                                        transforms.Resize(256),
                                        transforms.CenterCrop(224),                
                                        transforms.ToTensor(),
                                        transforms.Normalize(mean=MEAN, std=STD)])
            image_tensor = img_transforms(image).float()
            image_tensor = image_tensor.unsqueeze_(0)
            image_tensor.to(device)
            
            softmax = nn.Softmax(dim=1)
            output = model(Variable(image_tensor))
            pred = softmax(output[0].cpu()).detach().numpy()
            trash_idx = str(pred.argmax()+1)
            pred_class, confidence = TRASH_DICT[trash_idx], pred.max()
            
            if pred_class == annotation:
                correct += 1
    
            Number.append(number)
            Img_name.append(img_name)
            Class.append(annotation)
            Predict.append(pred_class)
            Correct.append(pred_class == annotation)
            Confidence.append('{:.3}'.format(confidence))
            number += 1
            
            print('>>> Predicted: {} | Label: {}\n'
                '>>> Correct: {} [{}]/[{}]\n'
                '>>> Confidence: {:.3}'
                .format(pred_class, annotation, pred_class == annotation, correct, total, confidence))
        except Exception as e:
            print(img_path)
            print(e)
    accuracy = correct/total*100
    f.write('test_result_{}.csv \n'.format(args.resume.split('/')[-1]))
    f.write('data: {} \n'.format(args.data_dir))
    f.write('accuracy : {} \n'.format(accuracy))
    f.close()

    print('\n>>> Accuracy: {:.3} [{}]/[{}]'.format(accuracy, correct, total))

    result['Number'] = Number
    result['Img_name'] = Img_name
    result['Class'] = Class
    result['Predict'] = Predict
    result['Correct'] = Correct
    result['Confidence'] = Confidence
    csv = pd.DataFrame(result, columns=['Number', 'Img_name', 'Class', 'Predict', 'Correct', 'Confidence'])
    csv.to_csv('test_result_{}.csv'.format(args.resume.split('/')[-1]),index=False)

def main():
    root_dir = 'data'
    infer_img_file = os.path.join(root_dir, 'test_index.txt')
    img_dirs = [x[0] for x in os.walk(os.path.join(root_dir,args.data_dir))][1:]
    img_dirs_dict = {}
    for img_dir in img_dirs:
        trash_name = img_dir.split('/')[-1]
        img_dirs_dict[trash_name] = img_dir
    
    infer_img_paths, infer_annos = [], []
    with open(infer_img_file, "r") as lines:
        for line in lines:
            img_name = line.split()[0]
            trash_idx = line.split()[1]
            infer_img_paths.append(os.path.join(img_dirs_dict[TRASH_DICT[trash_idx]], img_name))
            infer_annos.append(TRASH_DICT[trash_idx])
    assert len(infer_img_paths) == len(infer_annos)
    predict_images(infer_img_paths, infer_annos)

if __name__ == '__main__':
    main()